import sendgrid
from sendgrid.helpers.mail import *

sg = sendgrid.SendGridAPIClient(apikey='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')
from_email = Email("no-reply@warhorse.in")

# def sendConfirmationLink(mailId, confirmationId):
#     try:
#         to_email = Email(mailId)
#         subject = 'Confirm to play Dalal Bull!'
#         message = "<h4>Click <a href='{}confirm?_cId={}'>here</a> to confirm <br/> <br/> DalalBull <br/> Kurukshetra'17 Team</h4>".format(
#             Game.getMailAddress(), str(confirmationId))
#         print(message)
#         content = Content("text/html", message)
#         mail = Mail(from_email, subject, to_email, content)
#         response = sg.client.mail.send.post(request_body=mail.get())
#         print(response.status_code)
#         print(response.body)
#         print(response.headers)
#         return True
#     except Exception:
#         print('Error in sending mail')
#         return False


def sendForgotPasswordLink(mailId, forgotPasswordLink):
    try:
        to_email = Email(mailId)
        subject = 'Reset Password Link!'
        message = "<h4>Click <a href='admin.warhorse.in/changeAdminPassword?_fGI={}'>here</a> to change your password " \
                  "<br/><br/>Vishal<br/>Warhorse</h4>".format(str(forgotPasswordLink))

        content = Content("text/html", message)
        mail = Mail(from_email, subject, to_email, content)
        response = sg.client.mail.send.post(request_body=mail.get())
        print(response.status_code)
        print(response.body)
        print(response.headers)
        return True
    except Exception:
        print('Error in sending mail')
        return False


def sendUserConfirmationLink(mailID, confirmationLink):

    try:
        to_email = Email(mailID)
        subject = "Verification Email"
        message = "<h4>Click <a href='api.warhorse.in/userForgotPassword?_fGI={}'>here</a> to change your password " \
                  "<br/><br/>Vishal<br/>Warhorse</h4>".format(str(confirmationLink))
        content = Content("text/html", message)
        mail = Mail(from_email, subject, to_email, content)
        response = sg.client.mail.send.post(request_body=mail.get())
        print(response.status_code)
        return True
    except:
        print('Error in sending mail')
        return False


if __name__ == '__main__':
    sendForgotPasswordLink('sevenup13@gmail.com', '12334343343')