(function () {
    'use strict';

    angular.module('warhorse')
        .controller('homeController', homeController);

    homeController.$inject = ['$scope', 'homeService', '$route', '$state', '$rootScope', '$css'];


    function homeController($scope, homeService, $route, $state, $rootScope, $css) {
        var ctrl = this;
        $scope.isHidden = false;
        $rootScope.age = "18";
        $rootScope.location = "bbb";

        $css.bind({
            href: '../css/popup.css'
        }, $scope);


        homeService.home().then(function (response) {
         //   console.log(response.data);
            ctrl.ages = response.data.payload;
            ctrl.ageList = [];
            angular.forEach(ctrl.ages, function (value, key) {
                ctrl.ageList.push(key);
            });
            ctrl.ageList.sort();
         //   console.log(ctrl.ageList);

        })

        $scope.create = function (data) {
            $scope.isHidden = true;
            ctrl.locationList = [];
            $rootScope.age = ctrl.age;
            angular.forEach(ctrl.ages, function (value, key) {
                if (key === data) {
                    ctrl.locationList.push(value);
                }
            });
          //  console.log(ctrl.locationList[0]);
        }

        $scope.courses = function () {

            $rootScope.location = ctrl.location;
            $state.go('courses');
            $route.reload();

        }

        $(function () {
            $('.pop-up').hide();
            $('.pop-up').fadeIn(1000);
            $('.close-button').click(function (e) {
                $('.pop-up').fadeOut(700);
                $('#overlay').removeClass('blur-in');
                $('#overlay').addClass('blur-out');
                e.stopPropagation();
            });

        });

    }
})();