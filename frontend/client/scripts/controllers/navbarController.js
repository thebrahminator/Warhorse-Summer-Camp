(function () {
    'use strict';
    angular.module('warhorse')
        .controller('navbarController', navbarController);

    navbarController.$inject = ['$scope', '$css'];


    function navbarController($scope, $css) {

        $css.bind({
            href: '../css/home.css'
        }, $scope);

    }
})();