(function () {
    'use strict';

    angular.module('warhorse')
        .controller('coursesController', coursesController);

    coursesController.$inject = ['$scope', 'homeService', '$route', '$state', '$rootScope', '$css', '$timeout'];


    function coursesController($scope, homeService, $route, $state, $rootScope, $css, $timeout) {
        var ctrl = this;
        ctrl.age = $rootScope.age;
        ctrl.location = $rootScope.location;


        $scope.init = function () {

            if ($rootScope.age === "" || $rootScope.location === "" || $rootScope.age === undefined || $rootScope.location === undefined) {
                $state.go('home');
                $route.reload();
            }
        }

      //  console.log($rootScope.age + "Courses" + $rootScope.location)

        $css.bind({
            href: '../css/courses.css'
        }, $scope);


        ctrl.data = {
            "bundleid": "dfasfsfas"
        }


        homeService.course($rootScope.age, $rootScope.location).then(function (response) {

         //   console.log(response.data.payload);
            ctrl.bundles = response.data.payload;
          //  console.log(ctrl.bundles[0].id);
          //  console.log("PieceCost" + ctrl.bundles[0].pieceCost);
        });


        ctrl.payment = function (id) {

            if ($rootScope.age === "" || $rootScope.location === "" || $rootScope.age === undefined || $rootScope.location === undefined) {
                $state.go('home');
                $route.reload();
            } else {
                $state.go('payment');
                $route.reload();
                $rootScope.id = id;
              //  console.log("bundle-id", $rootScope.id);
            }
        }


        $('.slider').slick({
            slidesToShow: 3,
            slidesToScroll: 3,
            dots: true,
            infinite: true,
            cssEase: 'linear'
        });

    }

})();
