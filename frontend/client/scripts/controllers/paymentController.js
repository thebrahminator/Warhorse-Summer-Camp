(function () {
	    'use strict';

	    angular.module('warhorse')
	        .controller('paymentController', paymentController);

	    paymentController.$inject = ['$scope', 'homeService', '$route', '$state', '$rootScope', '$modal', '$css', '$window'];


	    function paymentController($scope, homeService, $route, $state, $rootScope, $modal, $css, $window) {
		            var ctrl = this;
		            ctrl.age = $rootScope.age;
		            ctrl.modalshow = false;

		            ctrl.imojolink = "";
		            ctrl.formdetails = {
				                "name": null,
				                "email": null,
				                "mobile": null,
				                "bundleID": $rootScope.id,
				                "courseID": "",
				                "batchID":""
				            }

		          //  console.log(ctrl.formdetails);

		            ctrl.location = $rootScope.location;
		            $scope.value = false;

		            if ($rootScope.age === "" || $rootScope.location === "" || $rootScope.age === undefined || $rootScope.location === undefined) {
				                $state.go('home');
				                $route.reload();
				              //  console.log("check")
				            }

//				            console.log($rootScope.age + "Payment" + $rootScope.location)

				            $css.bind({
						                href: '../css/payment.css'
						            }, $scope);

				            ctrl.bundleid = {
						                "bundleID":  $rootScope.id
						            }

				            console.log(ctrl.bundleid);
				            homeService.payment(ctrl.bundleid).then(function (response) {

						            //    console.log(response.data)
						             //   console.log(response.data.payload);
						                ctrl.bundledetails = response.data.bundleData;
						                ctrl.coursedetails = response.data.payload;

						            });

				            ctrl.bundleclick = function (imojolink) {
						                ctrl.formdetails.bundleID = $rootScope.id;
						                console.log(imojolink);
						                ctrl.imojolink = imojolink;
						            }

				            ctrl.individualclick = function (imojolink, courseid) {
						                ctrl.formdetails.courseID = courseid;
						           //     console.log(imojolink);
						            //    console.log(courseid);
						                ctrl.imojolink = imojolink;
						                ctrl.modalshow = true;
						            }


				            ctrl.formsubmit = function () {
						                if (ctrl.formdetails.name == null || ctrl.formdetails.email == null || ctrl.formdetails.mobile == null) {
									                alert("Enter the details");
									            }
						                else {
									              //  console.log("Form :", ctrl.formdetails);

									                homeService.detail(ctrl.formdetails).then(function (response) {

												             //       console.log(response);

												                });

									                $window.location.href = ctrl.imojolink;

									            }

						            }

				            $scope.dropme = function (index) {
						                $scope.activeParentIndex = index;
						                if ($scope.check)
							                    $scope.check = false;
						                else
							                    $scope.check = true;

						                $(".dd").toggleClass("glyphicon-chevron-down");
						                $(".dd").toggleClass("glyphicon-chevron-up");

						            };

				            $scope.isShowing = function (index) {
						                return $scope.activeParentIndex === index;
						            };

				            $(document).on('click', '.signup-popup2', function () {
						              //  console.log(ctrl.formdetails.courseID);
						                $("#myModal").show();

						            });

				            $(function () {
						                $(".signup-popup").on("click", function () {
									                $("#myModal ").show();
									            });

						                $(".course-data").click(function () {
									                $("#myModal-individual").hide();
									                $("#myModal").show();
									            });

						                $("#close-individual").click(function () {
									                $("#myModal-individual").hide();
									            });

						                $(".close--").click(function () {
									                $("#myModal").hide();
									            });

						            });

				      (function () {

					                  if (!String.prototype.trim) {
								                  (function () {
											                      var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
											                      String.prototype.trim = function () {
														                              return this.replace(rtrim, '');
														                          };
											                  })();
								              }

					                  [].slice.call(document.querySelectorAll('input.input__field')).forEach(function (inputEl) {
								                  if (inputEl.value.trim() !== '') {
											                      classie.add(inputEl.parentNode, 'input--filled');
											                  }
								                  inputEl.addEventListener('focus', onInputFocus);
								                  inputEl.addEventListener('blur', onInputBlur);
								              });

					                  function onInputFocus(ev) {
								                  classie.add(ev.target.parentNode, 'input--filled');
								              }

					                  function onInputBlur(ev) {
								                  if (ev.target.value.trim() === '') {
											                      classie.remove(ev.target.parentNode, 'input--filled');
											                  }
								              }
					              })();

				        }

})();
