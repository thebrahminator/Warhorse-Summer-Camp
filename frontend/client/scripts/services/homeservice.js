(function () {
    'use strict';

    angular.module('warhorse')
        .factory("homeService", homeService)

    homeService.$inject = ['$http'];

    function homeService($http) {

        var service = {};

        service.home = HomeFunction;
        service.course = CourseFunction;
        service.payment = PaymentFunction;
        service.detail = DetailFunction;

        return service;

        function HomeFunction() {
            return $http.get('https://api.warhorse.in/ageLocation').then(handleSuccess).catch(handleError);

        }

        function CourseFunction(age, location) {
            return $http.get('https://api.warhorse.in/bundleAndCourse?age=' + age + '&location=' + location).then(handleSuccess).catch(handleError);

        }

        function PaymentFunction(id) {
            return $http.post('https://api.warhorse.in/bundleDetails', id).then(handleSuccess).catch(handleError);

        }

        function DetailFunction(params) {
            return $http.post('https://api.warhorse.in/enterDetails', params).then(handleSuccess).catch(handleError);

        }

        function handleError(data) {
            return data;
        }

        function handleSuccess(data) {
            return data;
        }


    }
})();