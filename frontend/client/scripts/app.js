'use strict';

var states = [{
        name: 'home',
        state: {
            url: '/home',
            templateUrl: '../views/home.html',
            data: {
                text: 'HOME',
                visible: false
            }
        }
    },
    {
        name: 'courses',
        state: {
            url: '/courses',
            templateUrl: '../views/courses.html',
            data: {
                text: 'COURSES',
                visible: false
            }
        }
    },
    {
        name: 'payment',
        state: {
            url: '/payment',
            templateUrl: '../views/payment.html',
            data: {
                text: 'PAYMENT',
                visible: false
            }
        }
    }
];

var app = angular.module('warhorse', ['ui.router', 'ui.bootstrap','ngAnimate' ,'ngRoute', 'angularCSS', 'slick','ksSwiper','swiperRepeat'])
    .config(function($stateProvider, $urlRouterProvider, $locationProvider, $routeProvider) {

        $urlRouterProvider.otherwise('/home');
        states.forEach(function(state) {
            $stateProvider.state(state.name, state.state);
        })
    });
