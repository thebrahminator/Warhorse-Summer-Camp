import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import Routes from './router/routes'
import store from './store/store'

Vue.use(VueResource)
Vue.use(VueRouter)
Vue.use(BootstrapVue)

const router = new VueRouter ({
  routes:Routes,
  mode:'history'
});

new Vue({
  el: '#app',
  router:router,
  store,
  render: h => h(App)
});
