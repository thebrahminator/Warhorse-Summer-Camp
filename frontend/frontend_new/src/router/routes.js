import  home from '../components/home'
import courses from '../components/courses'
import payment from '../components/payment'

export default[
  {path:'/',component:home},
  {path:'/courses',component:courses},
  {path:'/payment',component:payment}
]
