from flask_script import Manager
from flask_migrate import MigrateCommand, Migrate
from SummerCamp import database, app

manager = Manager(app)
migrate = Migrate(app, database)
manager.add_command('database', MigrateCommand)

if __name__ == '__main__':
    manager.run()