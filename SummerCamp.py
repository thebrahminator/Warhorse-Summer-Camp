"""

This file contains the code for the overall admin side of the summer camp website, don't mess it up with normal code
that needs to go into the SummerCampAPI file.

This is saaaxxx, biatch.

"""
from flask import Flask, url_for, render_template, request, session, flash, send_file, redirect
from models import database, Admin, ForgotPasswordLink, Course, Location, Bundle, AgeLocation, Batch
from werkzeug.utils import secure_filename
from sqlalchemy import exc
import uuid
import os
import arrow
from SummerCampMail import sendForgotPasswordLink
import calendar
from functools import wraps
from InquiryRetrieval import inquiry
from prospectus import createProspectus
from BatchSelection.batchdisplay import batchdetails
from BatchSelection.manualadd import batchmanualadd
from CouponCode.coupon_code import coupon_code_bp
from mongoconfig import mongo
app = Flask(__name__)
app.secret_key="JinjaMaster"
#app.config["SQLALCHEMY_DATABASE_URI"] = 'postgresql://balajidr:spiderman@localhost:5432/summercamp'
ip = "127.0.0.01"
app.config['MONGO_URI'] = f"mongodb://thebrahminator:beyblade@{ip}:27017/batch"
app.config["SQLALCHEMY_DATABASE_URI"] = 'postgresql://thebrahminator:beyblade@localhost:5432/summercamp'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
ALLOWED_EXTENSIONS = ['jpg', 'png', 'jpeg', 'pdf']
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLDER = os.path.join(APP_ROOT, "static/")
app.config["UPLOAD_FOLDER"]= UPLOAD_FOLDER
app.config["ALLOWED_EXTENSIONS"] = ALLOWED_EXTENSIONS
app.register_blueprint(inquiry)
app.register_blueprint(createProspectus.prospectus)
app.register_blueprint(batchdetails)
app.register_blueprint(coupon_code_bp)
app.register_blueprint(batchmanualadd)
database.init_app(app)
mongo.init_app(app)
#================================================ ADMIN BLOCK STARTS ===================================================
@app.route('/getAdminRegister', methods=["GET"])
def getAdminRegister():
    '''
    <div class="col-xs-4">
        <a href="{{ url_for('getAdminRegister') }}" class="btn btn-danger btn-block">Register</a>
    </div>

    Code here whenever it has to be tested
    '''

    """
    :description:
    Works towards rendering the Admin Registration Form

    :return:
    template for registering Admin
    """

    return render_template('auth/register.html')

@app.route('/postAdminRegister', methods=["POST"])
def postAdminRegister():
    """
    :param
    id -> Unique ID for each Admin
    name -> Name of the admin
    uname -> Username of the admin, for login purpose
    email -> For sending work related mails
    :return:

    redirect to template rendered by Register.

    """
    id = str(uuid.uuid1())
    name = request.form.get('name', '')
    uname = request.form.get('username', '')
    email = request.form.get('email', '')
    password = request.form.get('password', '')

    addAdmin = Admin(id=id, name=name, uname=uname, password=password, email=email)
    try:
        database.session.add(addAdmin)
        database.session.commit()
        flash("Success")
        return redirect(url_for('getAdminRegister'))

    except exc.IntegrityError as e:
        print(e)
        flash("Invalid username, register again")
        return redirect(url_for('getAdminRegister'))


@app.route('/', methods=["GET"])
def getAdminLogin():

    if 'loggedIn' in session.keys():
        if session['loggedIn']:
            return redirect(url_for('dashboard'))

    return render_template('auth/login.html')


@app.route('/postAdminLogin', methods=["POST"])
def postAdminLogin():
    """
    :param
    uname -> for getting username
    password -> for getting the password

    :return:
    template according to what the output is

    """
    uname = request.form.get('uname', '')
    password = request.form.get('password', '')
    checkAdmin = Admin.query.filter_by(uname=uname).first()

    if not checkAdmin:
        flash("Invalid Username, Register Yourself")
        return redirect(url_for('getAdminLogin'))

    if not checkAdmin.checkDeleted():
        flash("Invalid access, check with Administrator")
        return redirect(url_for('getAdminLogin'))

    if not checkAdmin.checkPassword(password=password):
        flash("Wrong Password")
        return redirect(url_for('getAdminLogin'))

    session['isAdmin'] = True
    session['username'] = uname
    session['loggedIn'] = True

    return redirect(url_for('dashboard'))

@app.route('/dashboard', methods=["GET"])
def dashboard():

    if 'loggedIn' not in session.keys():
        flash("Login to perform further activities")
        return redirect(url_for('getAdminLogin'))

    elif not session['loggedIn']:
        flash("Login to perform further activities")
        return redirect(url_for('getAdminLogin'))

    return render_template('dashboard/homepage.html')

@app.route('/adminLogout')
def adminLogout():
    session['loggedIn'] = False
    flash("Successfully Logged Out")
    return redirect(url_for('getAdminLogin'))

@app.route('/getAdminForgotPassword', methods=["GET"])
def getAdminForgotPassword():
    return render_template('auth/forgotpassword.html')

@app.route('/postAdminForgotPassword', methods=["POST"])
def postAdminForgotPassword():
    """
        :param
        uname -> For getting the username

        :return:
        mail to the user, with reset link
        """
    uname = request.form.get('username', '')
    culprit = Admin.query.filter_by(uname=uname).first()
    doesExist = ForgotPasswordLink.query.filter_by(uname=uname).all()
    for record in doesExist:
        if record.status == 1:
            record.status = 2
            database.session.commit()

    if not culprit:
        flash("Wrong User Name")
        return redirect(url_for('getAdminLogin'))

    id = str(uuid.uuid1())
    status = 1
    culpritFP = ForgotPasswordLink(uname=uname, fptext=id, is_admin=True, status=1)
    database.session.add(culpritFP)
    database.session.commit()

    sendForgotPasswordLink(mailId=culprit.email, forgotPasswordLink=id)
    flash("Email sent to {}".format(culprit.email))
    return redirect(url_for('getAdminForgotPassword'))

@app.route('/changeAdminPassword', methods=["GET"])
def changeAdminPassword():
    """
    :param
    fgi -> forgot link intimation

    :return:
    template with a form to get new password

    """
    fgi = request.args.get('_fGI', '')
    forgotPassword = ForgotPasswordLink.query.filter_by(fptext=fgi).filter_by(status=1).all()
    print(len(forgotPassword))
    if forgotPassword:
        forgotPassword[0].updateStatus()
        database.session.commit()
        return render_template('auth/newpassword.html', uname=forgotPassword[0].uname)
    else:
        flash("Invalid Confirmation Link, Please check again")
        return redirect(url_for('getAdminLogin'))

@app.route('/postChangeAdminPassword', methods=["POST"])
def postChangeAdminPassword():
    username = request.form.get('uname', '')
    password = request.form.get('password', '')
    culprit = Admin.query.filter_by(uname=username).first()
    culprit.changePassword(password=password)
    database.session.commit()
    flash("Password Changed")
    return render_template('auth/login.html')

@app.route('/getAllAdmin', methods=["GET"])
def getAllAdmin():
    """
    :param
    No input required
    :return:
    template with all the admin details.

    """
    allAdmin = Admin.query.all()
    reversed(allAdmin)
    allAdminList = []
    for admin in allAdmin:
        allAdminList.insert(0, admin)

    return render_template('admin/allAdminList.html', allAdmins = allAdminList)

#================================================ ADMIN BLOCK ENDS =====================================================

#================================================ COURSE BLOCK STARTS ==================================================

@app.route('/addCourse')
def addCourse():

    if 'loggedIn' in session.keys():
       if session['loggedIn'] == True:
           return render_template('course/addcourse.html')
    flash("Login for using this feature")

    return redirect(url_for('getAdminLogin'))

def allowed_file(filename):
    return '.' in filename and filename.split('.')[1].lower() in ALLOWED_EXTENSIONS

@app.route('/postAddCourse', methods=["POST"])
def postAddCourse():
    """
    :param
    id -> ID of the course
    name -> Name of the course
    description -> Course description
    price -> The amount at which the course has to be sold.

    :return:
    redirection to the adding the course file

    """
    id = str(uuid.uuid1())
    name = request.form.get('name', '')
    description = request.form.get('description', '')
    price = request.form.get('price', ' ')
    prospectusLink = request.form.get('prospec', '')
    subtitle = request.form.get('subtitle', '')
    print(description)

    if 'poster' not in request.files:
        flash('No file uploaded')
        return redirect(url_for('addCourse'))

    poster = request.files['poster']

    print("Lolcats")

    if poster.filename == '':
        flash('No files selected')
        return redirect(url_for('addCourse'))
    filename = ""
    image_filename = ""
    if poster and allowed_file(poster.filename):
        extension = poster.filename.split('.')[1]
        print("Inside")
        image_filename = "images/" + name + "." + extension
        poster.save(os.path.join(app.config["UPLOAD_FOLDER"], image_filename))
        try:
            course = Course(id=id, name=name, image_name=image_filename, description=description, is_active=True, price=price,
                            prospectusLink=prospectusLink, subtitle=subtitle)
            database.session.add(course)
            database.session.commit()
            flash("Course has been added")
            return redirect(url_for('addCourse'))
        except Exception as e:
            print(e)
            print("Doesn't work")
            flash("This is something awesome")
            return redirect(url_for('addCourse'))

@app.route('/readCourse')
def readCourse():
    if 'loggedIn' not in session.keys():
        if not session['loggedIn']:
            flash("Enter the right credentials for logging in")
            return redirect(url_for('getAdminLogin'))

    allCourse = Course.query.all()
    reversed(allCourse)
    allCourseList = []
    for course in allCourse:
        allCourseList.insert(0, course)

    return render_template('course/allCourseList.html', allCourses=allCourseList)

@app.route('/changeCourseStatus', methods=["POST"])
def changeCourseStatus():

    if 'loggedIn' in session.keys():
        if session['loggedIn'] == True:
            id = request.form.get('id', '')
            course = Course.query.filter_by(id=id).first()
            course.deleteCourse()
            database.session.commit()
            flash("Successfully Changed Availability")
            return redirect(url_for('readCourse'))

    flash("Login to perform this function")
    return redirect(url_for('getAdminLogin'))

@app.route('/editCourse', methods=["GET"])
def editCourse():
    if 'loggedIn' in session.keys():
        if session['loggedIn'] == True:
            id = request.args.get('id', '')
            course = Course.query.filter_by(id=id).first()
            print(course)
            return render_template('course/editCourse.html', course=course)

    flash("Enter correct credentials to login")
    return redirect('auth/login.html')

@app.route('/postEditCourse', methods=["POST"])
def postEditCourse():
    """
    :argument
    id -> for uniquely identifying the record
    name -> name of the course
    description -> description of the course
    price -> rate of each course
    poster -> image of the poster
    :return:
    redirect to the original page
    """

    if 'loggedIn' in session.keys():
        if session['loggedIn'] == True:
            id = request.form.get('id', '')
            course = Course.query.filter_by(id=id).first()
            name = request.form.get('name', ' ')
            description = request.form.get('description', '')
            price = request.form.get('price', '')
            prospectusLink = request.form.get('prospec', '')
            subtitle = request.form.get('subtitle', '')

            #Checking if each of the field matches with earlier field, and changing it.
            if name != course.name:
                course.editName(name=name)
            if description != course.description:
                course.editDescription(description)
            if price != course.price:
                course.editPrice(price)

            if prospectusLink != course.prospectuslink:
                course.editProspect(prospectusLink=prospectusLink)

            if subtitle != course.subtitle:
                course.editSubtitle(subtitle=subtitle)

            print("Works till here")
            # Checking if the poster is there in the files
            if 'poster' not in request.files:
                database.session.commit()
                print("Skipped")
                flash("No poster image.")
                #return redirect(url_for('readCourse'))
            print("Lol")
            print(request.__dict__)

            if 'poster' in request.files:
                poster = request.files['poster']
                if poster.filename == '':
                    database.session.commit()
                    flash("Added other data, poster name invalid")
                else:
                    if poster and allowed_file(poster.filename):
                        extension = poster.filename.split('.')[1]
                        print("Inside")
                        filename = "images/" + name + "." + extension
                        poster.save(os.path.join(app.config["UPLOAD_FOLDER"], filename))
                        course.editImageLink(filename)
                        database.session.commit()
                        flash("Successfully updated deets")
                        return redirect(url_for('readCourse'))
                    else:
                        database.session.commit()
                        flash("Everything updated, invalid poster extension")
                        return redirect(url_for('readCourse'))

    flash("Login to perform this function")
    return redirect(url_for('getAdminLogin'))

#================================================ COURSE BLOCK ENDS ====================================================

#================================================ LOCATION BLOCK STARTS=================================================
@app.route('/addLocation', methods=["GET"])
def addLocation():
    if 'loggedIn' in session.keys():
        if session['loggedIn'] == True:
            return render_template('location/addLocation.html')

    flash("Login to perform this action")
    return redirect(url_for('getAdminLogin'))

@app.route('/postAddLocation', methods=["POST"])
def postAddLocation():
    """
    :param
    id -> to get the id of location
    name -> for identifying the overall location name
    generic_name -> for the generic name of the system
    address -> for the address of the location
    :return:

    Redirection to adding a location page.
    """

    id = str(uuid.uuid1())
    name = request.form.get('name', '')
    gname = request.form.get('gname', '')
    address = request.form.get('address', '')
    try:
        location = Location(id=id, location_name=name, generic_name=gname, address=address, is_active=True)
        database.session.add(location)
        database.session.commit()
        changeLiveLocationStatus()
    except exc.IntegrityError:
        flash("Error in storing data")
        return redirect(url_for('addLocation'))

    flash("Added Location Details. ")
    return redirect(url_for('addLocation'))

@app.route('/readLocation', methods=["GET"])
def readLocation():
    """
    :param
    None
    :return:
    rendered template of location
    """
    if 'loggedIn' not in session.keys():
        if not session['loggedIn']:
            flash("Enter the right credentials for logging in")
            return redirect(url_for('getAdminLogin'))

    allLocations = Location.query.all()
    reversed(allLocations)
    allLocationList = []
    for location in allLocations:
        allLocationList.insert(0, location)

    return render_template('location/allLocationList.html', allLocations=allLocationList)

@app.route('/changeLocationStatus', methods=["POST"])
def changeLocationStatus():
    """
    :param
    id -> id of the location, that needs to be changed
    :return:
    changed value, flash message and redirected to the readLocation page
    """
    if 'loggedIn' in session.keys():
        if session['loggedIn'] == True:
            id = request.form.get('id', '')
            location = Location.query.filter_by(id=id).first()
            location.changeLocationStatus()
            database.session.commit()
            changeLiveLocationStatus()
            flash("Updated all the Tables")
            return redirect(url_for('readLocation'))

    flash("Login to perform this action")
    return redirect(url_for('getAdminLogin'))

@app.route('/editLocation', methods=["GET"])
def editLocation():
    if 'loggedIn' not in session.keys():
        if not session['loggedIn']:
            flash("Login to perform this action")
            return redirect(url_for('getAdminLogin'))

    id = request.args.get('id', '')
    location = Location.query.filter_by(id=id).first()
    return render_template('location/editLocation.html', location=location)

@app.route('/postEditLocation', methods=["POST"])
def postEditLocation():
    """
    :param:
    id -> for checking the ID of the location
    location_name -> for the location name
    generic_name -> for the generic name of the location
    address -> for the overall address of the location

    :return:

    Redirecting to the original URL
    """
    if 'loggedIn' not in session.keys():
        if not session['loggedIn']:
            flash("Login to perform this action")
            return redirect(url_for('getAdminLogin'))

    id = request.form.get('id', '')
    location = Location.query.filter_by(id=id).first()
    location_name = request.form.get('name', '')
    generic_name = request.form.get('gname', '')
    address = request.form.get('address', '')

    if location.location_name != location_name:
        location.changeLocationName(location_name)

    if location.generic_name != generic_name:
        location.changeGenericName(generic_name)

    if location.address != address:
        location.changeAddress(address)

    try:
        database.session.commit()
        changeLiveLocationStatus()
    except exc.IntegrityError:
        flash("Cannot store in database")
        return redirect(url_for('editLocation', id=location.id))

    print(location)
    flash("Edited, yo!!")

    return redirect(url_for('editLocation', id=location.id))

#================================================ LOCATION BLOCK ENDS ==================================================
#================================================ BUNDLE BLOCK STARTS ==================================================
@app.route('/addBundle')
def addBundle():
    """
    :param:
    The Location and Course list, which is being offered.
    :return:
    Rendering the template of adding bundle
    """
    if 'loggedIn' not in session.keys():
        if not session['loggedIn']:
            flash("Login to perform this action")
            return redirect(url_for('getAdminLogin'))

    locations = Location.query.all()
    locationList = []
    for location in locations:
        if location.is_active == True:
            if location.location_name not in locationList:
                locationList.append(location.location_name)

    course = Course.query.all()
    return render_template('bundle/addBundle.html', locations=locationList, courses=course)

@app.route('/postAddBundle', methods=["POST"])
def postAddBundle():
    """
    :desc:
    This function is for adding the different details about a bundle into the db

    :param:
    id -> uniquly identifying the ID
    name -> name of the bundle
    start_age -> Starting age of the bundle
    end_age -> Ending age of the bundle
    location -> Location where its being offered
    price -> Price of the bundle
    course1 -> First course which is being offered
    course2 -> Second course which is being offered
    course3 -> Third course which is being offered (This can be null, or nothing)
    :return:
    Redirect to the original page
    """
    id = str(uuid.uuid1())
    name = request.form.get('name', '')
    start_age = int(request.form.get('sage', ''))
    end_age = int(request.form.get('eage', ''))
    location = request.form.get('location', '')
    price = request.form.get('price', '')
    course1 = request.form.get('course1', '')
    course2 = request.form.get('course2', '')
    course3 = request.form.get('course3', '')
    description = request.form.get('description', '')

    bundle = Bundle(id=id, name=name, start_age=start_age, end_age=end_age, course1=course1,
                    course2=course2, course3=course3, location=location, price=price, is_active=True,
                    description=description)

    value = addAgeLocation(start_age, end_age, location)
    print(value)
    try:
        database.session.add(bundle)
        database.session.commit()
        flash("Added bundle")
        return redirect(url_for('addBundle'))

    except exc.IntegrityError:
        flash("Unable to add the element")
        return redirect(url_for('addBundle'))

@app.route('/readBundle', methods=["GET"])
def readBundle():
    if 'loggedIn' not in session.keys():
        if not session['loggedIn']:
            flash("Enter the right credentials for logging in")
            return redirect(url_for('getAdminLogin'))

    allBundles = Bundle.query.all()
    print(allBundles)
    reversed(allBundles)
    bundleDetails = []
    for bundle in allBundles:
        bundleDict = {}

        bundleDict['name'] = bundle.name
        bundleDict['location'] = bundle.location
        bundleDict['start_age'] = int(bundle.start_age)
        bundleDict['end_age'] = bundle.end_age
        bundleDict['id'] = bundle.id
        bundleDict['price'] = bundle.price
        bundleDict['is_active'] = bundle.is_active
        bundleDict['courses'] = []
        bundleDict['description'] = bundle.bundle_description
        course1 = Course.query.filter_by(id=bundle.course1).first()
        course2 = Course.query.filter_by(id=bundle.course2).first()
        bundleDict['courses'].append(course1.name)
        bundleDict['courses'].append(course2.name)
        if bundle.course3.lower() != "nothing":
            course3 = Course.query.filter_by(id=bundle.course3).first()
            bundleDict['courses'].append(course3.name)
        else:
            bundleDict['courses'].append("Nothing")
        bundleDetails.append(bundleDict)

    return render_template('bundle/allBundleList.html', allBundles=bundleDetails)

@app.route('/readActiveBundle', methods=["GET"])
def readActiveBundle():
    if 'loggedIn' not in session.keys():
        if not session['loggedIn']:
            flash("Enter the right credentials for logging in")
            return redirect(url_for('getAdminLogin'))

    allBundles = Bundle.query.filter_by(is_active=True).all()
    courseNameList = []
    bundleDetails = []
    for bundle in allBundles:
        bundleDict = {}

        bundleDict['name'] = bundle.name
        bundleDict['location'] = bundle.location
        bundleDict['start_age'] = int(bundle.start_age)
        bundleDict['end_age'] = bundle.end_age
        bundleDict['id'] = bundle.id
        bundleDict['price'] = bundle.price
        bundleDict['is_active'] = bundle.is_active
        bundleDict['courses'] = []
        bundleDict['description'] = bundle.bundle_description
        course1 = Course.query.filter_by(id=bundle.course1).first()
        course2 = Course.query.filter_by(id=bundle.course2).first()
        bundleDict['courses'].append(course1.name)
        bundleDict['courses'].append(course2.name)
        if bundle.course3.lower() != "nothing":
            course3 = Course.query.filter_by(id=bundle.course3).first()
            bundleDict['courses'].append(course3.name)
        else:
            bundleDict['courses'].append("Nothing")
        bundleDetails.append(bundleDict)

    return render_template('bundle/allActiveBundles.html', allBundles=bundleDetails)

@app.route('/deleteBundle', methods=["POST"])
def deleteBundle():

    if 'loggedIn' not in session.keys():
        if not session['loggedIn']:
            flash("Enter the right credentials for logging in")
            return redirect(url_for('getAdminLogin'))

    id = request.form.get('id', '')
    bundle = Bundle.query.filter_by(id=id).first()
    bundle.changeBundleStatus()
    database.session.commit()
    flash("Changed the Bundle Status")
    return redirect(url_for('readBundle'))

#================================================ BUNDLE BLOCK ENDS ====================================================
#================================================ BATCH BLOCK STARTS ===================================================
@app.route('/addBatch', methods=["GET"])
def addBatch():
    if 'loggedIn' not in session.keys():
        if session['loggedIn'] != True:
            flash("Login to access the credentials")
            return redirect(url_for('getAdminLogin'))

    location = Location.query.all()
    courses = Course.query.filter_by(is_active=True).all()
    locations = Location.query.filter_by(is_active=True).all()

    locationNameList = []
    locationList = []
    courseNameList = []
    for location in locations:
        if location.is_active:
            if location.location_name not in locationNameList:
                locationNameList.append(location.location_name)
            locationList.append(location)

    for course in courses:
        if course.is_active:
            courseNameList.append(course)

    return render_template('batch/addBatch.html', locations=locationList,
                           locationNameList=locationNameList, courseNameList=courseNameList)

@app.route('/postAddBatch', methods=["POST"])
def postAddBatch():
    id = str(uuid.uuid1())
    locid = request.form.get('address', '')
    courseid = request.form.get('course', '')
    start_age = int(request.form.get('sage', ''))
    end_age = int(request.form.get('eage', ''))
    price = request.form.get('price', '')
    total_seats = int(request.form.get('totseat', ''))
    start_date = request.form.get('bsd', '')
    end_date = request.form.get('bed', '')
    start_time = request.form.get('bst', '')
    end_time = request.form.get('bet', '')
    rem_seat = total_seats
    print(rem_seat)
    batch = Batch(id=id, course_id=courseid, center_id=locid, start_age=start_age, end_age=end_age, price=price,
                  total_seats=total_seats, remaining_seats=rem_seat, batch_start_date=start_date,
                  batch_end_date=end_date, is_completed=False, batch_start_time=start_time, batch_end_time=end_time)
    location = Location.query.filter_by(id=locid).first()
    addAgeLocation(start_age, end_age, location.location_name)
    try:
        database.session.add(batch)
        database.session.commit()
        flash("Successfully Entered details")
        return redirect(url_for('addBatch'))
    except exc.IntegrityError or exc.DataError:
        flash("Enter the details again. Problem Saving details")
        return redirect(url_for('addBatch'))

@app.route('/readBatch', methods=["GET"])
def readBatch():
    if 'loggedIn' not in session.keys():
        if not session['loggedIn']:
            flash("Login to Continue using the application")
            return redirect(url_for('getAdminLogin'))
        flash("Login to Continue using the application")
        return redirect(url_for('getAdminLogin'))

    batch = Batch.query.all()
    courseList = []
    locationList = []
    dateList = []
    for bat in batch:
        print(bat.id)
        course = Course.query.filter_by(id=bat.course_id).first()
        location = Location.query.filter_by(id=bat.center_id).first()
        startDate = getDateTime(arrow.get(bat.batch_start_date, 'D/M/YYYY h:mm A'),
                                arrow.get(bat.batchstarttime, 'h:mm A'))
        dateList.append(startDate)
        courseList.append(course)
        locationList.append(location)
    return render_template('batch/allBatchList.html', batchAndCourse=zip(batch, courseList, locationList, dateList))

@app.route('/batchDetail', methods=["GET"])
def batchDetail():
    if 'loggedIn' not in session.keys():
        if not session['loggedIn']:
            flash("Login to Continue using the application")
            return redirect(url_for('getAdminLogin'))
        flash("Login to Continue using the application")
        return redirect(url_for('getAdminLogin'))

    batchId = request.args.get('id', '')
    print(batchId)
    batch = Batch.query.filter_by(id=batchId).first()
    course = Course.query.filter_by(id=batch.course_id).first()
    location = Location.query.filter_by(id=batch.center_id).first()
    startDate = arrow.get(batch.batch_start_date, 'D/M/YYYY h:mm A')
    endDate = arrow.get(batch.batch_end_date, 'D/M/YYYY h:mm A')

    startString = convertToString(startDate)
    endString = convertToString(endDate)

    return render_template('batch/checkDetails.html', batch=batch,course=course, location=location,
                           startString=startString, endString=endString)

@app.route('/readActiveBatch', methods=["GET"])
def readActiveBatch():
    if 'loggedIn' not in session.keys():
        if not session['loggedIn']:
            flash("Login to Continue using the application")
            return redirect(url_for('getAdminLogin'))
        flash("Login to Continue using the application")
        return redirect(url_for('getAdminLogin'))

    batch = Batch.query.filter_by(is_completed=False).all()
    courseList = []
    locationList = []
    dateList = []
    for bat in batch:
        course = Course.query.filter_by(id=bat.course_id).first()
        location = Location.query.filter_by(id=bat.center_id).first()
        startDate = getDateTime(arrow.get(bat.batch_start_date, 'D/M/YYYY h:mm A'),
                                arrow.get(bat.batchstarttime, 'h:mm A'))
        dateList.append(startDate)
        courseList.append(course)
        locationList.append(location)
    return render_template('batch/allBatchList.html', batchAndCourse=zip(batch, courseList, locationList, dateList))

@app.route('/deleteBatch', methods=["POST"])
def deleteBatch():
    if 'loggedIn' not in session.keys():
        if not session['loggedIn']:
            flash("Login to Continue using the application")
            return redirect(url_for('getAdminLogin'))
        flash("Login to Continue using the application")
        return redirect(url_for('getAdminLogin'))

    id = request.form.get('id', '')
    batch = Batch.query.filter_by(id=id).first()
    batch.changeBatchStatus()
    database.session.commit()

    return redirect(url_for('batchDetail', id=batch.id))



    

#================================================ BATCH BLOCK ENDS =====================================================

#================================================ USER BLOCK STARTS ====================================================

#================================================ USER BLOCK ENDS ======================================================
#================================================ HELPER FUNCTIONS =====================================================
def addAgeLocation(startage, endage, location):
    """

    :param startage: For the starting age
    :param endage: For the ending age
    :param location: For the location of the event.
    :return: 1 if successful, 2 if failure

    """
    print("Locats")
    agelocations = AgeLocation.query.all()
    print(agelocations)

    if not agelocations:
        id = str(uuid.uuid1())
        try:
            print("Inside This Enter")
            enterAgeLocation = AgeLocation(id=id, start_age=startage, end_age=endage, location=location, is_active=True)
            database.session.add(enterAgeLocation)
            database.session.commit()
            return 1
        except:
            pass

    for agelocation in agelocations:
        if startage == agelocation.start_age and endage == agelocation.end_age and location == agelocation.location:
            print("Inside if")
            print("Everything is same.")
            return 2

    id = str(uuid.uuid1())
    try:
        print("Inside Enter")
        enterAgeLocation = AgeLocation(id=id, start_age=startage, end_age=endage, location=location, is_active=True)
        database.session.add(enterAgeLocation)
        database.session.commit()
        return 1

    except:
        pass
    return 2

@app.route('/testing')
def testing():
    changeLiveLocationStatus()
    return "This is sexx"

def changeLiveLocationStatus():
    """
    :param:
    When all the centres in a particular area are deleted, then there is no way we can send a proper front end call.
    Thus, this function makes sure that the live location doesn't get affected.
    :return:
    Nothing, Biatch

    """
    locations = Location.query.filter_by(is_active=True).all()
    locationList = []
    for location in locations:
        if location.is_active:
            locationList.append(location.location_name)
    agelocations = AgeLocation.query.all()
    bundles = Bundle.query.all()
    batches = Batch.query.all()

    for agelocation in agelocations:
        if agelocation.location in locationList and agelocation.is_active == False:
            agelocation.changeLocationStatus()
            database.session.commit()
        if agelocation.location not in locationList and agelocation.is_active == True:
            agelocation.changeLocationStatus()
            database.session.commit()

    for bundle in bundles:
        if bundle.location in locationList and bundle.is_active == False:
            bundle.changeBundleStatus()
            database.session.commit()
        if bundle.location not in locationList and bundle.is_active == True:
            bundle.changeBundleStatus()
            database.session.commit()

    for batch in batches:
        location = Location.query.filter_by(id=batch.center_id).first()

        if location.location_name in locationList and batch.is_completed ==True and batch.center_id == location.id:
            batch.changeBatchStatus()
            database.session.commit()

        if location.location_name not in locationList and batch.is_completed == False and batch.center_id == location.id:
            batch.changeBatchStatus()
            database.session.commit()

def convertToString(endDate):
    endString = str(endDate.date().day) + ' ' + calendar.month_name[endDate.date().month] + ' ' + \
                str(endDate.time().hour) + ':' + str(endDate.time().minute)

    if endDate.time().hour <= 12:
        endString = endString + ' ' + 'AM'
    else:
        endString = endString + ' ' + 'PM'

    return endString

def getDateTime(start_date, start_time):

    endString = f"{str(start_date.date().day)} {calendar.month_name[start_date.date().month]}, \t {str(start_time.time().hour)}" \
                f":{str(start_time.time().minute)}"

    if start_time.time().hour <= 12:
        endString = endString + ' ' + 'AM'
    else:
        endString = endString + ' ' + 'PM'

    return endString


def bundleCourseNameChange():
    pass
if __name__ == '__main__':
    app.run(debug=True, port=5000)
