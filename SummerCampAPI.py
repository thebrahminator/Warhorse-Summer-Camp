"""

This file is used for running the API that'll be linked with the front end application. It has nothing to do with the
Admin Dashboard that's being built. Make sure you don't mess up the code in both the files.

This is batshit crazy, biatch.

"""
from flask import Flask, url_for, render_template, request, session, flash, send_file, redirect, jsonify
from flask_cors import CORS
from models import database, AgeLocation, Bundle, Course, Batch, Location, Inquiry, Prospectus
from helperClasses import User, CouponCode
from sqlalchemy import exc
import arrow, calendar
import uuid
import os
from SiteUser.SiteUser import siteuser
from BatchSelection.batchselection import batchselection
from Razorpay.razorpayinteg import razorpay_blueprint
from InquiryRetrieval import inquiry
from mongoconfig import mongo


app = Flask(__name__)
CORS(app)
app.secret_key="JinjaMaster"
app.config['MONGO_DBNAME'] = "batch"
ip = "127.0.0.01"
app.config['MONGO_URI'] = f"mongodb://thebrahminator:beyblade@{ip}:27017/batch"
app.config["SQLALCHEMY_DATABASE_URI"] = 'postgresql://thebrahminator:beyblade@localhost:5432/summercamp'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
app.register_blueprint(siteuser)
app.config["BASE_URL"] = "api.warhorse.in/static/"
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLDER = os.path.join(APP_ROOT, "static/files")
app.config["UPLOAD_FOLDER"]= UPLOAD_FOLDER
database.init_app(app)
mongo.init_app(app=app)
app.register_blueprint(batchselection)
app.register_blueprint(razorpay_blueprint)
app.register_blueprint(inquiry)

@app.route('/ageLocation')
def index():
    """

    :return:
    Json of age, and Location
    """

    agelocations = AgeLocation.query.all()

    ageLocDict = {}
    for agelocation in agelocations:
        if agelocation.is_active == False:
            continue
        ageString = str(agelocation.start_age)+'-'+str(agelocation.end_age)
        if ageString not in ageLocDict.keys():
            ageLocDict[ageString] = [agelocation.location]
        else:
            ageLocDict[ageString].append(agelocation.location)
        #print(ageLocDict)

    print(ageLocDict)

    if not ageLocDict:
        return jsonify({
            'code': 404,
            'message': "No data found",
            'payload': {}
        })

    payloadDict = {
        'code': 200,
        'message': 'Data is in Payload',
        'payload': ageLocDict
    }

    return jsonify(payloadDict)

@app.route('/bundleAndCourse', methods=["GET"])
def bundleAndCourse():

    age = request.args.get('age', '')
    location = request.args.get('location')
    ageList = age.split('-')
    ageList[0] = int(ageList[0])
    ageList[1] = int(ageList[1])
    print(ageList)
    bundles = Bundle.query.filter_by(start_age=ageList[0]).filter_by(end_age=ageList[1]).all()
    bundleAndCourseDict = {}
    bundleAndCourseList = []
    bundleAndCourseDict['code'] = 200
    bundleAndCourseDict['messge'] = "Request Success"

    for bundle in bundles:
        print(location, bundle.location)
        if bundle.location.lower() != location.lower() or not bundle.is_active:
            continue
        if bundle.course3.lower() != "nothing":
            print(bundle.course1)
            course1 = Course.query.filter_by(id=bundle.course1).first()
            course2 = Course.query.filter_by(id=bundle.course2).first()
            course3 = Course.query.filter_by(id=bundle.course3).first()
            print("This is 1: ", course1)
            print("This is 2: ", course2)
            print("This is 3: ", course3)
            locationString1 = app.config["BASE_URL"]+ course1.image_name
            locationString2 = app.config["BASE_URL"] + course2.image_name
            locationString3 = app.config["BASE_URL"] + course3.image_name
            bundleDict= {}
            bundleDict['id'] = bundle.id
            bundleDict['isBundle'] = True
            bundleDict['courses'] = [course1.name, course2.name, course3.name]
            bundleDict['price'] = bundle.price
            bundleDict['images'] = [locationString1, locationString2, locationString3]
            bundleDict['courseCost'] = [course1.price, course2.price, course3.price]
            bundleDict['name'] = bundle.name
            bundleDict['pieceCost'] = course1.price + course2.price + course3.price
            bundleDict['description'] = bundle.bundle_description
            bundleAndCourseList.append(bundleDict)
        else:
            course1 = Course.query.filter_by(id=bundle.course1).first()
            course2 = Course.query.filter_by(id=bundle.course2).first()
            locationString1 = app.config["BASE_URL"] + course1.image_name
            locationString2 = app.config["BASE_URL"] + course2.image_name
            bundleDict = {}
            bundleDict['id'] = bundle.id
            bundleDict['isBundle'] = True
            bundleDict['courses'] = [course1.name, course2.name]
            bundleDict['price'] = bundle.price
            bundleDict['courseCost'] = [course1.price, course2.price]
            bundleDict['images'] = [locationString1, locationString2]
            bundleDict['name'] = bundle.name
            bundleDict['pieceCost'] = course1.price + course2.price
            bundleDict['description'] = bundle.bundle_description
            bundleAndCourseList.append(bundleDict)


    batches = Batch.query.filter_by(start_age=ageList[0]).filter_by(end_age=ageList[1]).all()
    print(batches)
    coursesList = []
    for batch in batches:

        course = Course.query.filter_by(id=batch.course_id).first()
        print("Course: "+ course.name)
        location_req = Location.query.filter_by(id=batch.center_id).first()
        print("l "+ location_req.location_name.lower())
        print(location)

        if location_req.location_name.lower() != location.lower():
            print("Different locations")
            continue

        if not course.is_active and not location_req.is_active:
            print("Comes inside")
            continue

        if course.name in coursesList:
            print("Same shit, yo!")
            continue

        courseDict = {}
        courseDict['isBundle'] = False
        courseDict['id'] = course.id
        courseDict['name'] = course.name
        coursesList.append(course.name)
        print(coursesList)
        courseDict['price'] = course.price
        courseDict['image'] = app.config["BASE_URL"] + course.image_name
        bundleAndCourseList.append(courseDict)

    bundleAndCourseDict['payload'] = bundleAndCourseList

    if not bundleAndCourseList:
        return jsonify({
            "code" : 404,
            "message": "No Data Found",
            "payload": {}
        })

    return jsonify(bundleAndCourseDict)

@app.route('/bundleDetails', methods=["POST"])
def bundleDetails():

    request1 = request.get_json()
    print(request1)
    bundleId = request1['bundleID']
    bundle = Bundle.query.filter_by(id=bundleId).first()

    if not bundle:
        payloadDict = {
            'code': 404,
            'message': "This bundle doesn't exist"
        }
        return jsonify(payloadDict)
    if bundle.course3.lower() != "nothing":
        courses = [
        Course.query.filter_by(id=bundle.course1).first(),
        Course.query.filter_by(id=bundle.course2).first(),
        Course.query.filter_by(id=bundle.course3).first()
        ]
        comboString = f"{courses[0].name} + {courses[1].name} + {courses[2].name}"
        bundleCost = courses[0].price + courses[1].price + courses[2].price
    else:
        courses = [
            Course.query.filter_by(id=bundle.course1).first(),
            Course.query.filter_by(id=bundle.course2).first()
        ]
        comboString = f"{courses[0].name} + {courses[1].name}"
        bundleCost = courses[0].price + courses[1].price

    coursePayloadList = []
    for course in courses:
        courseDict = {}
        courseDict['id'] = course.id
        courseDict['name'] = course.name
        courseDict['subtitle'] = course.subtitle
        courseDict['description'] = course.description
        courseDict['imageLink'] = app.config["BASE_URL"]+course.image_name
        courseDict['price'] = course.price

        prospectus = Prospectus.query.filter_by(courseid=course.id).filter_by(start_age=bundle.start_age).\
            filter_by(end_age=bundle.end_age).first()

        if prospectus:
            courseDict['prospectusLink'] = 'https://'+app.config["BASE_URL"]+prospectus.prospectuspdflink
        else:
            courseDict['prospectusLink'] = "#"

        # if course.prospectuslink:
        #     courseDict['prospectusLink'] = course.prospectuslink
        # else:
        #     courseDict['prospectusLink'] = "#"

        coursePayloadList.append(courseDict)

    #print(coursePayloadList)
    batches = Batch.query.filter_by(start_age=bundle.start_age).filter_by(end_age=bundle.end_age).all()
    addressList = []
    for batch in batches:

        if batch.is_completed:
            print("Sarakku")
            continue

        location = Location.query.filter_by(id=batch.center_id).first()
        print(location)
        course = Course.query.filter_by(id=batch.course_id).first()
        print(course)
        for courseData in coursePayloadList:
            print('l'+location.location_name)
            print(bundle.location)
            if course.name == courseData['name'] and location.location_name == bundle.location:
                print("Matched")
                if 'batches' not in courseData.keys():
                    if location.address not in addressList:
                        addressList.append(location.address)
                    start_date = arrow.get(batch.batch_start_date, 'D/M/YYYY h:mm A')
                    end_date = arrow.get(batch.batch_end_date, 'D/M/YYYY h:mm A')
                    if batch.remaining_seats != 0:
                        isActive = True
                    else:
                        isActive = False

                    courseData['batches'] = [{
                        'startDate': convertToString(start_date),
                        'endDate': convertToString(end_date),
                        'isActive': isActive,
                        'startTime': batch.batchstarttime,
                        'endTime': batch.batchendtime,
                        'batchID': batch.id

                    }]


                else:
                    if location.address not in addressList:
                        addressList.append(location.address)
                    start_date = arrow.get(batch.batch_start_date, 'D/M/YYYY h:mm A')
                    end_date = arrow.get(batch.batch_end_date, 'D/M/YYYY h:mm A')
                    if batch.remaining_seats != 0:
                        isActive = True
                    else:
                        isActive = False

                    tempDict = {
                        'startDate': convertToString(start_date),
                        'endDate': convertToString(end_date),
                        'isActive': isActive,
                        'startTime': batch.batchstarttime,
                        'endTime': batch.batchendtime,
                        'batchID': batch.id
                    }
                    courseData['batches'].append(tempDict)
            else:
                pass

    for courseData in coursePayloadList:
        if 'batches' not in courseData.keys():
            courseData['batches'] = "No Batches Available"

    ageGroup = str(bundle.start_age)+'-'+str(bundle.end_age)

    if not coursePayloadList:
        payloadDict = {
            'code': 205,
            'message': "No courses available"
        }
        return jsonify(payloadDict)

    couponCodes = CouponCode.query.all()
    allCouponCodeList_1 = []
    for couponCode in couponCodes:
        if couponCode.is_active == False:
            continue

        couponDict = {}
        couponDict['couponkey'] = couponCode.coupcode
        couponDict['amount'] = couponCode.amount
        couponDict['id'] = couponCode.id
        allCouponCodeList_1.append(couponDict)

    payloadDict = {
        'code': 200,
        'message': "Data is in the payload",
        'payload': coursePayloadList,
        'bundleData': {
            'bundleName': bundle.name,
            'bundlePrice': bundle.price,
            'bundleAgeGroup': ageGroup,
            'bundleLocation': bundle.location,
            'bundeleAddress': addressList,
            'bundleComboString': comboString,
            'bundleDescription': bundle.bundle_description,
            'bundleTotalCost': bundleCost,
            'bundleID': bundle.id,
            'couponCodeList': allCouponCodeList_1
        }
    }

    return jsonify(payloadDict)

#================================================ USER CONFIG STARTS ===================================================
@app.route('/enterDetails', methods=["POST"])
def enterDetails():
    request1 = request.get_json()

    name = request1['name']
    mobile = request1['mobile']
    email = request1['email']
    bundleID = request1['bundleID']
    courseID = request1['courseID']
    batchID = request1['batchID']
    id = str(uuid.uuid1())
    inquiry = Inquiry(name=name, mobile=mobile, email=email, bundleID=bundleID, courseID=courseID,id=id)
    try:
        database.session.add(inquiry)
        database.session.commit()
    except:
        return jsonify({
            "code": 403,
            "message": "Unable to enter the data",
            "payload": {}
        })

    return jsonify({
        'code': 200,
        'message': "Successfully Entered the details",
        'payload': []
    })

@app.route('/userRegister',methods=['POST'])
def userRegister():
    """
    id -> Unique ID for each User
    name -> Name of the user
    password -> password of the user, for login purpose
    email -> For sending account related mails
    mobile -> Mobile number of the user
    :return:
    json with data
    """
    request1 = request.get_json()
    id = str(uuid.uuid1())
    name = request1['name']
    email = request1['email']
    password = request1['password']
    mobile = request1['mobile']

    if not id or not name or not email or not password or not mobile:
        return jsonify({
            'code': 403,
            'message': "Incomplete Data, Please Enter Again",
            'payload': {}
        })
    addUser = User(id=id, name=name, mobile=mobile, password=password, email=email, is_active=0)
    try:
        database.session.add(addUser)
        database.session.commit()
        return jsonify(
                {   "status":"200",
                    "message":"Registered Successfully",
                    "payload":{}
             })

    except exc.IntegrityError as e:
        print(e)
        return jsonify({

            "code":"400",
            "message":"Registration Failed",
            "payload":{}

        })



@app.route('/userLogin',methods=['POST'])
def userLogin():
    """
    email -> Email of the user
    password -> password of the user, for login purpose
    email -> For logging in the user
    :return:
    json data according to what the function does.
    """
    email = request.form.get('email', '')
    password = request.form.get('password', '')
    user = User.query.filter_by(email=email).first()

    if not user:
        return jsonify({
            'code': 400,
            'message': "Incorrect Mail ID"
        })

    if not user.checkPassword(password=password):
        return jsonify({
            'code': 400,
            'message' :"Incorrect Password"
        })

    if user.is_active == 0:
        return jsonify({
            'code': 400,
            'message': "Verify email, to continue."
        })
    session['loggedIn'] = True
    session['userId'] = user.id

    return jsonify({

        "code":201,
        "message":"Successfully Logged in",
        "payload":{}

    })

#================================================ HELPER FUNCTIONS =====================================================

def convertToString(enteredDate):
    endString = str(enteredDate.date().day) + ' ' + calendar.month_name[enteredDate.date().month]

    return endString

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5001, debug=True)
