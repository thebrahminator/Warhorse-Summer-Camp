from flask import Blueprint, render_template, redirect, url_for, flash, send_file, request
from models import Inquiry, Bundle, Course, database
from helperClasses import PopupRequest
import pandas as pd
import os
import uuid, json
inquiry = Blueprint('inquiry', __name__)

@inquiry.route('/checkInquiry')
def checkInquiry():
    inquiries = Inquiry.query.all()
    print(inquiries)
    responseQuery = []
    for inq in inquiries:

        print(vars(inq))
        respDict = {}
        print(inq.bundleID)
        try:
            bundle = Bundle.query.filter_by(id=inq.bundleID).first()
            course = Course.query.filter_by(id=inq.courseID).first()
        except:
            flash("Error with loading")
            return redirect(url_for('getAdminLogin'))
        
        if bundle:
            respDict['bundleName'] = bundle.name
        else:
            respDict['bundleName'] = None
        if course:
            respDict['courseName'] = course.name
        else:
            respDict['courseName'] = None
        respDict['userName'] = inq.name
        respDict['mobileNo'] = inq.mobile
        respDict['emailID'] = inq.email
        responseQuery.append(respDict)

    return render_template('inquiry/displayInquiry.html', participants=responseQuery)

@inquiry.route('/downloadInquiries')
def downloadInquiries():
    inquiries = Inquiry.query.all()
    print(inquiries)
    responseQuery = []
    for inq in inquiries:
        respDict = {}
        try:
            bundle = Bundle.query.filter_by(id=inq.bundleID).first()
            course = Course.query.filter_by(id=inq.courseID).first()
        except:
            flash("Error with loading")
            return redirect(url_for('getAdminLogin'))

        if bundle:
            respDict['bundleName'] = bundle.name
        else:
            respDict['bundleName'] = None
        if course:
            respDict['courseName'] = course.name
        else:
            respDict['courseName'] = None
        respDict['userName'] = inq.name
        respDict['mobileNo'] = inq.mobile
        respDict['emailID'] = inq.email
        responseQuery.append(respDict)

    pathname = os.path.dirname(__file__) + '/static/files/inquiry.csv'
    respDF = pd.DataFrame(responseQuery)
    respDF.to_csv(pathname, index=False)
    return send_file(pathname, as_attachment=True)

@inquiry.route('/postpopupreq', methods=["POST"])
def postpopupreq():

    popupreq = request.get_json()
    student_name = popupreq['student_name']
    parent_name = popupreq['parent_name']
    parent_email = popupreq['parent_email']
    parent_mobile = popupreq['parent_mobile']
    student_age = popupreq['student_age']
    id = str(uuid.uuid1())
    popreq = PopupRequest(student_name=student_name, student_age=student_age, parent_mail=parent_email,
                          parent_name=parent_name, parent_mobile=parent_mobile, id=id)
    try:

        database.session.add(popreq)
        database.session.commit()
        message = {
            'code': 200,
            'message': "Successfully inserted into the DB"
        }
        return json.dumps(message)

    except Exception as e:
        print(e)
        message = {
            'code': 400,
            'message': "Unable to enter into the DB"
        }
        return json.dumps(message)

@inquiry.route('/getpopupdetails', methods=["GET"])
def getPopupDetails():

    popreq = PopupRequest.query.all()
    return render_template('inquiry/displaypopup.html', participants=popreq)

@inquiry.route('/downloadpopupdetails', methods=["GET"])
def downloadPopupDetails():

    popreq = PopupRequest.query.all()

    popuplist = []

    for data in popreq:
        respDict = {}
        respDict['student_name'] = data.student_name
        respDict['student_age'] = data.student_age
        respDict['parent_name'] = data.parent_name
        respDict['parent_email'] = data.parent_mail
        respDict['parent_mobile'] = data.parent_mobile
        popuplist.append(respDict)

    pathname = os.path.dirname(__file__) + '/static/files/popup.csv'
    respDF = pd.DataFrame(popuplist)
    respDF.to_csv(pathname, index=False)

    return send_file(pathname, as_attachment=True)



