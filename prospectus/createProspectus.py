from flask import Blueprint, redirect, request, render_template, session, flash, url_for
from flask import current_app as app
from models import Course, database
from helperClasses import Prospectus
import uuid
import os
prospectus = Blueprint('prospectus', __name__, url_prefix='/prospectus')
ALLOWED_EXTENSIONS = ['pdf']

@prospectus.route('/create', methods=["GET"])
def createProspectus():

    if 'loggedIn' not in session.keys():
        flash('Login to continue')
        return url_for('getAdminLogin')

    if session['loggedIn'] == False:
        flash('Login to continue')
        return redirect(url_for('getAdminLogin'))

    courses = Course.query.all()
    return render_template('course/createProspectus.html', courses=courses)

@prospectus.route('/upload', methods=["POST"])
def uploadProspectus():

    id = str(uuid.uuid1())
    courseId = request.form.get('courseID', '')
    start_age = int(request.form.get('sage', ''))
    end_age = int(request.form.get('eage', ''))

    if 'prospectusPDF' not in request.files:
        flash("Prospectus PDF Not uploaded")
        return redirect(url_for('createProspectus'))

    prospectuspdf = request.files['prospectusPDF']

    if prospectuspdf.filename == "":
        flash("No files selected")
        return redirect(url_for('createProspectus'))

    filename = ""

    if prospectuspdf and allowed_file(prospectuspdf.filename):
        course = Course.query.filter_by(id=courseId).first()
        extension = prospectuspdf.filename.split('.')[1]
        pdf_filename = f"files/{course.name}_{start_age}_{end_age}.{extension}"
        prospectuspdf.save(os.path.join(app.config["UPLOAD_FOLDER"], pdf_filename))

        try:
            prospectuspdf = Prospectus(id=id, prospectuspdflink=pdf_filename,
                                       courseID=course.id, start_age=start_age, end_age=end_age)
            database.session.add(prospectuspdf)
            database.session.commit()
            flash("Successfully added PDF")
            return redirect(url_for('prospectus.createProspectus'))

        except Exception as e:
            print(e)
            flash("Unable to add PDF")
            return redirect(url_for('prospectus.createProspectus'))


def allowed_file(filename):
    return '.' in filename and filename.split('.')[1].lower() in ALLOWED_EXTENSIONS