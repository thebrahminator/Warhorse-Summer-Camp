from flask import Blueprint, request, render_template, redirect, url_for
from helperClasses import User

siteuser = Blueprint('siteuser', __name__, url_prefix='/user')

@siteuser.route('/register')
def userRegistration():
    userData = request.get_json()

    name = userData['name']
    email = userData['email']
    password = userData['password']
    mobile = userData['mobile']

