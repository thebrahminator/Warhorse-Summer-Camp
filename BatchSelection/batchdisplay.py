from flask import Blueprint, session, request, render_template
import json
from models import BatchSelectMap, Inquiry
from helperClasses import Students
from mongoconfig import mongo

batchdetails = Blueprint('batchdetails', __name__, url_prefix='/batch/display')

@batchdetails.route('/details', methods=["GET"])
def displayDetails():

    batchID = request.args.get('batchID', '')
    batches = BatchSelectMap.query.filter_by(batchID=batchID).all()
    batch_mongo = mongo.db.batches
    batch_list = batch_mongo.find()
    participant_list: list = []

    for batch_data in batch_list:
        batch_details = batch_data['batchDetails']
        print(batch_data)
        print(batch_data['parentID'])
        parent_details = Inquiry.query.filter_by(id=batch_data['parentID']).first()
        for batch_detail in batch_details:
            if batch_detail['batchID'] == batchID:
                participant_dict: dict = {
                    'schoolName': batch_detail['schoolName'],
                    'studentName': batch_detail['studentName'],
                    'parentName': parent_details.name,
                    'parentEmail': parent_details.email,
                    'parentMobile': parent_details.mobile
                }
                participant_list.append(participant_dict)

    student_details = Students.query.filter_by(batch_id=batchID).all()
    for student_detail in student_details:
        participant_dict: dict = {
            'schoolName': student_detail.school_name,
            'studentName': student_detail.student_name,
            'parentName': student_detail.parent_name,
            'parentEmail': student_detail.parent_mail,
            'parentMobile': student_detail.parent_mobile
        }
        participant_list.append(participant_dict)

    return render_template('batch/participantDetails.html', participantList=participant_list)


