from flask import Blueprint, request, session, render_template, flash, redirect, url_for
import json
from models import Inquiry, database, Batch, BatchSelectMap, Bundle, Location, Course
from helperClasses import Students
import uuid
import arrow
from Mailers.clientConfirmationMail import clientMail
import calendar
batchmanualadd = Blueprint('batchmanualadd', __name__, url_prefix='/batchManual')

@batchmanualadd.route('/getData', methods=["GET"])
def getData():

    batch_id = request.args.get('batchID', '')
    batchdetails = Batch.query.filter_by(id=batch_id).first()

    return render_template('batch/addParticipant.html', batch_details = batchdetails)

@batchmanualadd.route('/postData', methods=["POST"])
def postData():

    batch_id = request.form.get('id', '')
    student_name = request.form.get('sname', '')
    parent_name = request.form.get('pname', '')
    parent_email = request.form.get('pmail', '')
    parent_mobile = request.form.get('pmobile', '')
    school_name = request.form.get('sschool', '')
    id = str(uuid.uuid1())

    batch_details = Batch.query.filter_by(id=batch_id).first()
    course_details = Course.query.filter_by(id=batch_details.course_id).first()
    location_details = Location.query.filter_by(id=batch_details.center_id).first()
    date_and_time = getDateTime(arrow.get(batch_details.batch_start_date, 'D/M/YYYY h:mm A'),
                                arrow.get(batch_details.batchstarttime, 'h:mm A'))
    retData = clientMail(mailID=parent_email, course_name=course_details.name, location_name=location_details.address,
                         date_and_time=date_and_time)
    print(retData)

    student_details = Students(id=id, student_name=student_name, parent_mobile=parent_mobile, parent_name=parent_name,
                               parent_mail=parent_email, school_name=school_name, batch_id=batch_id)
    try:
        database.session.add(student_details)
        database.session.commit()

    except Exception as e:
        print(e)
        flash("Unable to add data point")
        return redirect(url_for('batchmanualadd.getData'))

    batch_details.remaining_seats = batch_details.remaining_seats - 1
    database.session.commit()

    flash("Successfully added data")
    return redirect(url_for('batchmanualadd.getData'))

def getDateTime(start_date, start_time):

    endString = f"{str(start_date.date().day)} {calendar.month_name[start_date.date().month]}, \t {str(start_time.time().hour)}" \
                f":{str(start_time.time().minute)}"

    if start_time.time().hour <= 12:
        endString = endString + ' ' + 'AM'
    else:
        endString = endString + ' ' + 'PM'

    return endString