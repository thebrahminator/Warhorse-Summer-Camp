from flask import Blueprint, request, session
import json
from models import Inquiry, database, Batch, BatchSelectMap, Bundle, Location, Course
from mongoconfig import mongo
import uuid
import arrow
from Mailers.clientConfirmationMail import clientMail
import calendar
batchselection = Blueprint('batchselection', __name__, url_prefix='/batch')

"""
Batch selection Schema

ID -> For uniquely identifying the 
Parent ID-> Unique student ID for distinguishing which kid is coming
isBundle -> If the parent has asked for bundle data.
Bundle ID -> If the student is registering as a bundle
AgeGroup -> Student Age Group
Location -> Overall location of the bundle

Batch Details:
    Batch ID -> For the batch that is being selected
    SchoolName -> For getting the school name of the student
    Name -> Name of the student
    Address-> Adress of where the class is going to take place    
    
DateTimeCreated -> For knowing when the entry was created.

"""

"""
Incoming Payload:
    {   
        "courseID": 
        "bundleID":
        "isBundle":
        "couponID":
        "name":
        "mobile":
        "email":
        "batchDetails":[
            "batchID":
            "studentName":
            "schoolName":
        ]
        "isPaid": 
    }
"""

@batchselection.route('/select', methods=["POST"])
def selectBatches():

    batchDetails = request.get_json()
    print(batchDetails)
    batchIDs = batchDetails['batchDetails']
    id = str(uuid.uuid1())
    inquiry = Inquiry(id=id, courseID=batchDetails['courseID'], bundleID=batchDetails['bundleID'], name=batchDetails['name'],
                      email=batchDetails['email'], mobile=batchDetails['mobile'])

    try:
        database.session.add(inquiry)
        database.session.commit()
    except Exception as e:
        print(e)
        message = {
            'code': 400,
            'message': "Unable to add the inquiry details"
        }
        return json.dumps(message)

    bundle = Bundle.query.filter_by(id=batchDetails['bundleID']).first()
    print(bundle.id)
    ageGroup = str(bundle.start_age) + '-' + str(bundle.end_age)


    dataSaved = {

        'id': str(uuid.uuid1()),
        'parentID': id,
        'bundleID': batchDetails['bundleID'],
        'isBundle': batchDetails['isBundle'],
        'dateCreated': str(arrow.utcnow().shift(hours=+5, minutes=+30)),
        'ageGroup': ageGroup,
        'ispaid': batchDetails['isPaid']
    }

    if batchDetails['isPaid'] == False:

        dataSaved['Location'] = bundle.location + " not_paid"
    else:

        batchDetails['isPaid'] = bundle.location

    batchesList = []

    for batchId in batchIDs:
        individualBatchDict = {}
        batch = Batch.query.filter_by(id=batchId['batchID']).first()
        batch.remaining_seats = batch.remaining_seats - 1
        database.session.commit()
        tempID = str(uuid.uuid1())
        course = Course.query.filter_by(id=batch.course_id).first()
        courseName = course.name
        try:
            dSID = dataSaved['id']
            batchselectionMap = BatchSelectMap(id=tempID, batchID=batch.id, selectID=dSID)
            database.session.add(batchselectionMap)
            database.session.commit()

        except Exception as e:
            print(e)
            message = {
                'code': 400,
                'message': "Unable to add Batch Selection Map"
            }
            return json.dumps(message)

        location = Location.query.filter_by(id=batch.center_id).first()
        locationName = location.address
        date_and_time = getDateTime(arrow.get(batch.batch_start_date, 'D/M/YYYY h:mm A'), arrow.get(batch.batchstarttime, 'h:mm A'))
        retData = clientMail(mailID=batchDetails['email'], course_name=courseName, location_name=locationName,
                             date_and_time=date_and_time)
        print(retData)
        individualBatchDict['batchID'] = batch.id
        individualBatchDict['studentName'] = batchId['studentName']
        individualBatchDict['schoolName'] = batchId['schoolName']
        individualBatchDict['address'] = location.address
        batchesList.append(individualBatchDict)

    dataSaved['batchDetails'] = batchesList

    batches = mongo.db.batches
    try:
        batches.insert(dataSaved)
        message = {
            'code': 200,
            'message': "Successfully inserted into the DB"
        }
        return json.dumps(message)
    except Exception as e:
        print(e)
        message = {
            'code': 400,
            'message': "Unable to enter into the DB"
        }
        return json.dumps(message)


def getDateTime(start_date, start_time):

    endString = f"{str(start_date.date().day)} {calendar.month_name[start_date.date().month]}, \t {str(start_time.time().hour)}" \
                f":{str(start_time.time().minute)}"

    if start_time.time().hour <= 12:
        endString = endString + ' ' + 'AM'
    else:
        endString = endString + ' ' + 'PM'

    return endString

