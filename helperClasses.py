from werkzeug.security import check_password_hash, generate_password_hash
import arrow
from models import database

class User(database.Model):

    __tablename__ = "siteuser"

    id = database.Column(database.String(128), unique=True, primary_key=True)
    name = database.Column(database.String(128), nullable=False)
    password = database.Column(database.String(128), nullable=False)
    email = database.Column(database.String(128), nullable=False, unique=True)
    mobile = database.Column(database.String(128), nullable=False, unique=True)
    is_active = database.Column(database.Integer)

    def __init__(self, id, name, password, email, mobile, is_active):
        self.id = id
        self.name = name
        self.email = email
        self.password = self.saltPassword(password=password)
        self.mobile = mobile
        self.is_active = is_active

    def saltPassword(self, password):

        return generate_password_hash(password=password)

    def checkPassword(self, password):

        return check_password_hash(self.password, password)


    def changePassword(self, password):
        self.password = self.saltPassword(password)

class Prospectus(database.Model):

    __tablename__ = 'prospectus'

    id = database.Column(database.String(128), unique=True, primary_key=True)
    courseid = database.Column(database.String(128))
    prospectuspdflink = database.Column(database.String(128), unique=True)
    start_age = database.Column(database.Integer)
    end_age = database.Column(database.Integer)
    created_at = database.Column(database.String(128))
    edited_at = database.Column(database.String(128))

    def __init__(self, id, courseID, prospectuspdflink, start_age, end_age):
        self.id = id
        self.prospectuspdflink = prospectuspdflink
        self.courseid = courseID
        self.start_age = start_age
        self.end_age = end_age
        self.created_at = str(arrow.utcnow().shift(hours=+5,minutes=+30))
        self.edited_at = str(arrow.utcnow().shift(hours=+5, minutes=+30))

    def editProspectus(self):
        self.edited_at = str(arrow.utcnow().shift(hours=+5, minutes=+30))


class CouponCode(database.Model):

    __tablename__ = 'couponcode'

    id = database.Column(database.String(128), unique=True, primary_key=True)
    coupcode = database.Column(database.String(128), unique=True)
    amount = database.Column(database.Integer)
    no_of_access = database.Column(database.Integer)
    is_active = database.Column(database.Boolean, unique=False)

    def __init__(self, id, coupcode, amount):
        self.id = id
        self.coupcode = coupcode
        self.amount = amount
        self.is_active = True
        self.no_of_access = 0

    def changeCouponCodeStatus(self):
        self.is_active = not self.is_active


class Students(database.Model):

    __tablename__ = 'students'

    id = database.Column(database.String(128), unique=True, primary_key=True)
    batch_id = database.Column(database.String(128))
    student_name = database.Column(database.String(128))
    parent_name = database.Column(database.String(128))
    parent_mobile = database.Column(database.String(128))
    school_name = database.Column(database.String(128))
    parent_mail = database.Column(database.String(128))

    def __init__(self, id, student_name, batch_id, parent_name, parent_mobile, parent_mail, school_name):
        self.id = id
        self.student_name = student_name
        self.parent_mail = parent_mail
        self.parent_name = parent_name
        self.parent_mobile = parent_mobile
        self.school_name = school_name
        self.batch_id = batch_id

class PopupRequest(database.Model):

    __tablename__ = 'popup'

    id = database.Column(database.String(128), unique=True, primary_key=True)
    student_name = database.Column(database.String(128))
    parent_name = database.Column(database.String(128))
    parent_mobile = database.Column(database.String(128))
    parent_mail = database.Column(database.String(128))
    student_age = database.Column(database.String(128))

    def __init__(self, id, student_name, parent_name, parent_mobile, parent_mail, student_age):
        self.id = id
        self.student_name = student_name
        self.parent_mail = parent_mail
        self.parent_name = parent_name
        self.parent_mobile = parent_mobile
        self.student_age = student_age