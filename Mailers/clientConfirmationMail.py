import sendgrid
from sendgrid.helpers.mail import *
from Mailers.html_to_pdf import mailerTemplate
sg = sendgrid.SendGridAPIClient(apikey='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')
from_email = Email("no-reply@warhorse.in")

def clientMail(mailID, course_name, location_name, date_and_time):
    try:
        to_email = Email(mailID)
        subject = "Summer for the Future -- Confirmation Mail"
        message = mailerTemplate(courseName=course_name, locationName=location_name, dateAndTime=date_and_time)
        content = Content("text/html", message)
        mail = Mail(from_email, subject, to_email, content)
        response = sg.client.mail.send.post(request_body=mail.get())
        print(response.status_code)
        return True
    except:
        print('Error in sending mail')
        return False

if __name__ == '__main__':
    clientMail('rajalakshmibabu@hotmail.com', course_name='Public Speaking',
               location_name='Kanchana Paati, 39/1,39/2, C.V. Raman Road, Alwarpet, Chennai, 600018',
               date_and_time='25 April 9:00 AM')