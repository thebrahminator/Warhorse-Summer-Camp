import jinja2
import os

def mailerTemplate(courseName, locationName, dateAndTime):
    fileName = os.path.join(os.path.dirname(__file__),'html_to_mail.html')
    filename = 'html_to_mail.html'
    html = open(fileName)
    html_template = jinja2.Template(html.read())
    rendered = html_template.render(locationName=locationName, courseName=courseName, date_and_time=dateAndTime)
    print(rendered)
    return rendered

if __name__ == '__main__':

    mailerTemplate(courseName="Vishal", locationName="Kilpauk", dateAndTime="12th May 2018, 11:30PM")