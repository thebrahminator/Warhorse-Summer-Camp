import flask_sqlalchemy
from werkzeug.security import generate_password_hash, check_password_hash
import arrow
database = flask_sqlalchemy.SQLAlchemy()
from helperClasses import User, Prospectus

class Inquiry(database.Model):

    __tablename__ = "inquiry"

    id = database.Column(database.String(128), primary_key=True)
    name = database.Column(database.String(128))
    email = database.Column(database.String(128))
    mobile = database.Column(database.String(128))
    bundleID = database.Column(database.String(128))
    courseID = database.Column(database.String(128))

    def __init__(self, id, name, email, mobile, bundleID, courseID):
        self.id = id
        self.name = name
        self.email = email
        self.mobile = mobile
        self.bundleID = bundleID
        self.courseID = courseID

class Admin(database.Model):
    """
    Admin is the maintainer of the entire Infrastructure.
    """

    __tablename__ = "admin"

    id = database.Column(database.String(128), primary_key = True, unique=True)
    name = database.Column(database.String(128), nullable=False)
    uname = database.Column(database.String(128), unique=True)
    password = database.Column(database.String(128), nullable=False)
    email = database.Column(database.String(128), nullable=False)
    is_active = database.Column(database.Boolean, unique=False, default=True)

    def __init__(self, id, name, uname, password, email):
        self.id = id
        self.name = name
        self.uname = uname
        self.email = email
        self.password = self.saltPassword(password=password)
        self.is_active = True

    def saltPassword(self, password):

        return generate_password_hash(password=password)

    def checkPassword(self, password):

        return check_password_hash(self.password, password)

    def checkDeleted(self):

        return self.is_active

    def changePassword(self, password):
        self.password = self.saltPassword(password)

class ForgotPasswordLink(database.Model):

    __tablename__ = "forgotpassword"

    fptext = database.Column(database.String(128), unique=True, primary_key=True)
    uname = database.Column(database.String(128))
    status = database.Column(database.Integer())
    is_admin = database.Column(database.Boolean, default=True)

    def __init__(self, fptext, uname, status, is_admin):
        self.uname = uname
        self.fptext = fptext
        self.status = status
        self.is_admin = is_admin

    def updateStatus(self):
        self.status = 2
        return 1

class Course(database.Model):

    __tablename__ = "course"

    id = database.Column(database.String(128), unique=True, primary_key=True)
    name = database.Column(database.String(128), nullable=False, unique=True)
    subtitle = database.Column(database.String(1024))
    description = database.Column(database.String(1024), nullable=False)
    image_name = database.Column(database.String(128), nullable=False)
    is_active = database.Column(database.Boolean, unique=False, default=True, nullable=False)
    price = database.Column(database.Integer, unique=False, nullable=False)
    prospectuslink = database.Column(database.String(256))

    def __init__(self, id, name, description, image_name, is_active, price, prospectusLink, subtitle):

        self.id = id
        self.name = name
        self.description = description
        self.image_name = image_name
        self.price = price
        self.is_active = is_active
        self.subtitle = subtitle
        self.prospectuslink = prospectusLink

    def deleteCourse(self):
        self.is_active = not self.is_active
        return 1

    def editName(self, name):
        self.name = name
        return 1

    def editDescription(self, description):
        self.description = description
        return 1

    def editPrice(self, price):
        self.price = price

    def editProspect(self, prospectusLink):
        self.prospectuslink = prospectusLink

    def editImageLink(self, imagelink):
        self.image_name = imagelink

    def editSubtitle(self, subtitle):
        self.subtitle = subtitle

class Location(database.Model):

    __tablename__ = "location"

    id = database.Column(database.String(128), unique=True, primary_key=True)
    location_name = database.Column(database.String(128), nullable=False)
    generic_name = database.Column(database.String(128), nullable=False)
    address = database.Column(database.String(128), nullable=False)
    is_active = database.Column(database.Boolean, unique=False, default=True, nullable=False)

    def __init__(self, id, location_name, generic_name, address, is_active):
        self.id = id
        self.location_name = location_name
        self.address = address
        self.generic_name = generic_name
        self.is_active = is_active

    def changeLocationStatus(self):
        self.is_active = not self.is_active

    def changeLocationName(self, location_name):
        self.location_name = location_name

    def changeGenericName(self, generic_name):
        self.generic_name = generic_name

    def changeAddress(self, address):
        self.address = address


class Bundle(database.Model):

    __tablename__ = "bundle"

    id = database.Column(database.String(128), unique=True, primary_key=True)
    name = database.Column(database.String(128), nullable=False)
    price = database.Column(database.Integer, nullable=False)
    bundle_description = database.Column(database.String(1024))
    course1 = database.Column(database.String(128), nullable=False)
    course2 = database.Column(database.String(128), nullable=False)
    course3 = database.Column(database.String(128))
    is_active = database.Column(database.Boolean, unique=False, default=True, nullable=False)
    location = database.Column(database.String(128), nullable=False)
    start_age = database.Column(database.Integer(), nullable=False)
    end_age = database.Column(database.Integer(), nullable=False)

    def __init__(self, id, name, price, course1, course2, course3, is_active, start_age,
                 location, end_age, description):
        self.id = id
        self.name = name
        self.price = price
        self.course1 = course1
        self.course2 = course2
        self.course3 = course3
        self.is_active = is_active
        self.start_age = start_age
        self.end_age = end_age
        self.location = location
        self.bundle_description = description

    def changeBundleStatus(self):
        self.is_active = not self.is_active

class AgeLocation(database.Model):

    __tablename__ = "agelocation"

    id = database.Column(database.String(128), unique=True, primary_key=True)
    start_age = database.Column(database.Integer, nullable=False)
    end_age = database.Column(database.Integer, nullable=False)
    location = database.Column(database.String(128), nullable=False)
    is_active = database.Column(database.Boolean, unique=False, nullable=False, default=True)

    def __init__(self, id, start_age, end_age, location, is_active):
        self.id = id
        self.start_age = start_age
        self.end_age = end_age
        self.location = location
        self.is_active = is_active

    def changeLocationStatus(self):
        self.is_active = not self.is_active

class Batch(database.Model):

    __tablename__ = "batch"

    id = database.Column(database.String(128), unique=True, primary_key=True)
    course_id = database.Column(database.String(128), nullable=False)
    center_id = database.Column(database.String(128), nullable=False)
    start_age = database.Column(database.Integer, nullable=False)
    end_age = database.Column(database.Integer, nullable=False)
    total_seats = database.Column(database.Integer, nullable=False)
    remaining_seats = database.Column(database.Integer, nullable=False)
    is_completed = database.Column(database.Boolean, unique=False, nullable=False, default=False)
    price = database.Column(database.Integer, nullable=False)
    batch_start_date = database.Column(database.String(128), nullable=False)
    batch_end_date = database.Column(database.String(128), nullable=False)
    batchstarttime = database.Column(database.String(128), nullable=False)
    batchendtime = database.Column(database.String(128), nullable=False)

    def __init__(self, id, course_id, center_id, start_age, end_age, total_seats, price,
                 remaining_seats, is_completed, batch_start_date, batch_end_date, batch_start_time, batch_end_time):
        self.id = id
        self.course_id = course_id
        self.center_id = center_id
        self.start_age = start_age
        self.end_age = end_age
        self.total_seats = total_seats
        self.price=price
        self.remaining_seats = remaining_seats
        self.is_completed = is_completed
        self.batch_start_date = batch_start_date
        self.batch_end_date = batch_end_date
        self.batchstarttime = batch_start_time
        self.batchendtime = batch_end_time

    def changeBatchStatus(self):
        self.is_completed = not self.is_completed

class pomodoro(database.Model):

    __tablename__ = "pomodoro"

    id = database.Column(database.String(128), unique=True, primary_key=True)

class BatchSelectMap(database.Model):

    __tablename__ = "batchselectmap"

    id = database.Column(database.String(128), primary_key=True)
    batchID = database.Column(database.String(128))
    selectionID = database.Column(database.String(128))

    def __init__(self, id, batchID, selectID):
        self.id = id
        self.batchID = batchID
        self.selectionID = selectID



