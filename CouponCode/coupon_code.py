from flask import Blueprint, redirect, render_template, session, flash, url_for, request
import uuid
from helperClasses import CouponCode
from models import database
coupon_code_bp = Blueprint('coupon_code_bp', __name__, url_prefix='/coupon_code')

@coupon_code_bp.route('/getCoupCode')
def getCoupCode():
    if 'loggedIn' in session.keys():
        if session['loggedIn'] == True:
            return render_template('coupon_code/generateCouponCode.html')

    flash("Enter the details")
    return redirect(url_for('getAdminLogin'))

@coupon_code_bp.route('/postCoupCode', methods=["POST"])
def postCoupCode():

    coupCode = request.form.get('coupCode', '')
    amount = request.form.get('amount', '')
    id = str(uuid.uuid1())
    coupon_code_temp = CouponCode(id=id, coupcode=coupCode, amount=amount)
    try:
        database.session.add(coupon_code_temp)
        database.session.commit()
    except Exception as e:
        print(e)
        flash("Error while storing the coupon")
        return redirect(url_for('coupon_code_bp.getCoupCode'))

    flash("Finished entering the coupon code")
    return redirect(url_for('coupon_code_bp.getCoupCode'))

@coupon_code_bp.route('/allCouponCodeList')
def allCouponCodeList():

    coupCodes = CouponCode.query.all()
    all_coupon_code_list = []
    for coupCode in coupCodes:
        all_coupon_code_list.append(coupCode)

    return render_template('coupon_code/allCouponCodeList.html', allCouponCodeList=all_coupon_code_list)

@coupon_code_bp.route('/deleteCouponCode', methods=["POST"])
def deleteCouponCode():

    couponCode = request.form.get('id', '')
    coupCode = CouponCode.query.filter_by(id=couponCode).first()
    try:
        coupCode.changeCouponCodeStatus()
        database.session.commit()
    except Exception as e:
        print(e)
        flash("Error while editing the coupon")
        return redirect(url_for('coupon_code_bp.allCouponCodeList'))

    flash("Finished editing the coupon code")
    return redirect(url_for('coupon_code_bp.allCouponCodeList'))
